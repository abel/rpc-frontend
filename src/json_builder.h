#pragma once

#include <ostream>
#include <unordered_map>
#include <memory>

class Parser;
class Frontend;

using TypeOption = std::unordered_map<std::string, std::string>;

extern TypeOption CppTypeOptions;      // Type option for cplusplus
extern TypeOption ProtobufTypeOptions; // Type option for Protobuf
extern TypeOption CSharpTypeOptions;   // Type option for CSharp
extern TypeOption GolangTypeOptions;   // Type option for Golang
extern TypeOption IdlTypeOptions;      // Type option for IDL

namespace Json {
class Value;
}

// JSON builder
class JsonBuilder {
  Frontend *frontend_{nullptr};
  std::unique_ptr<Json::Value> root_;

public:
  // ctor
  JsonBuilder(Frontend *frontend);
  // dtor
  ~JsonBuilder();
  // Build a JSON object and write to os
  // @param idlfile idl file name, using for collecting common structs
  // @param parser Parser
  // @param os output stream
  // @param typeOption User type options for different target
  // @retval true success
  // @retval false failure
  bool build(const std::string &ildfile, const Parser &parser, std::ostream &os,
             const TypeOption &typeOptions);
  // Get Json string from last build
  std::string get_string();
};
